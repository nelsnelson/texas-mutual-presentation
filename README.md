# Presentation

I have been invited to speak to a panel of employees at Texas Mutual Insurance Company.

## Plan

Presentation:

### Introductions - five minutes

[Introduce myself.]

- Hi, my name is Nels Nelson.  I enjoy writing and operating software, and I have had the good fortune to do that for a living since around 2004.

- May I ask the panel to introduce themselves? Shall we go in last name alphabetical order?

- How long have you each worked at Texas Mutual?

- I'd certainly like to get to know each of you more, but we only have five minutes for intros.

- Before I begin, I want to quickly gauge the experience you have using GitLab or similar tools.

- May I ask what are your favorite tools that you use for developing software?

### Presentation - thirty minutes

[The purpose of this presentation is to teach the audience about a topic in which I have specialized knowledge.]

#### Preface - one minute

[Address my nervousness and discomfort with public speaking.]

[1 minute]

- In the interest of transparency and honesty, which I consider to be personal values of mine, I think it's important for me to say that I struggled quite a bit in preparing for this interview. I have always been very nervous about public speaking, as I'm sure are most people except for the most experienced speakers. And personally, I've always preferred to listen more than I talk.

- After specializing in mostly software development in Java for the first few years of my career, I quickly realized that the world of technology was vast and infinite, and I'd really like to be more of a generalist and seek opportunities to learn about a lot of different things.

- Please take advantage of this EtherPad: https://etherpad.wikimedia.org/p/YK1nI380thDhZsi-mKZK

- It can be used to add questions for me as we go.

#### Thesis statement - two and a half minutes

[Tell the audience what the topic is.]

[2.5 minutes]

- The topic of this presentation today is:

- GitLab self-managed installations

- When considering what to present here, I felt it was important to show something useful, relevant, and practical.  There are a lot of things that I have some intermediate-level expertise in.  But I don't have a Master's Degree or a PhD.  Even if I did, I still think it's important for me to be humble about any level of expertise I might have.  So what would be interesting and useful in the context of a presentation like this to a captive audience?

- I'd like to speak to you today about using a self-managed GitLab standalone installation to manage changes, testing, and deployments for software projects.

- I got to use this software a lot over the last four and a half years.  One of the operating principles at GitLab is dog-fooding their product whenever possible, which was really nice, because they were trying to build a product that did a lot of stuff for a lot of different roles in the practice of software development.

- And while I am by no means an expert with the software, I still use it every day, and I'm constantly learning new things about it, and new ways it can be used.

- GitLab is pretty special, in many ways, but mostly because of the fact that its comprehensive suite of tools support the entire software development lifecycle. It _includes_ tools for project planning, source code management, continuous integration and continuous deployment (CI/CD), monitoring, and security, all within a single interface of a web application.

- All of these things supports teams with collaborating more efficiently and streamlining their workflows. This integration reduces the need for multiple disjointed tools.

- It enables a more unified and automated approach to software development.

- Today I'll be going over:

  - An overview of GitLab
  - Setting up a self-managed GitLab installation
  - Managing changes with GitLab
  - Testing and CI/CD with GitLab

#### Content

[Commence presentation. Get to it!]

##### GitLab overview - two minutes

[Give a brief overview of GitLab.]

[2 minutes]

- What is GitLab?

- GitLab is at least two things, aside from the company.

  1. GitLab is a web-based software service product to which companies and teams can subscribe for a monthly or annual rate
  2. GitLab is also a self-managed software application which can run on your own hardware or cloud resources.

- What are some key features of GitLab?  I personally use GitLab to store and manage the code for my software projects.  I also use it to support CI/CD for my projects.

- It includes tools for tracking issues, managing tasks, and planning projects with features like issue boards, milestones, labels, and time tracking.

- It includes a feature called Auto DevOps which can automatically configure software development lifecycle processes, from building and testing to deployment and monitoring, reducing the complexity of setting up the CI/CD pipeline with some best practice patterns.

- It includes security tooling for static and dynamic security testing, dependency scanning, and container scanning to identify vulnerabilities.

- It integrates monitoring with Prometheus for observing application performance.

- It provides a wiki system for documentation and knowledge sharing.

- It provides a Docker container registry and it has built-in Kubernetes integration for orchestrating containerized applications.

- There are many other features you can read about in more detail here: https://about.gitlab.com/features/

##### Setting up a self-managed GitLab installation

[Demonstrate setting up a self-managed GitLab installation on a virtual machine Ubuntu system.]

[5 minutes]

- The system requirements for GitLab are pretty small for the most basic functionality.

  - For more details see: https://docs.gitlab.com/ee/install/requirements.html#hardware-requirements

- Here, let us try to set up a quick GitLab instance.

##### Managing changes with GitLab

[Demonstrate how changes can be managed using our GitLab installation.]

[5 minutes]



##### Testing and CI/CD with GitLab

[Demonstrate how tests may be ran as individual jobs within a stage, and how tests can gate automatic deployments.]

[5 minutes]



### Q&A - thirty minutes

[Open the discussion to questions related to the topic presented.]

- Here is that EtherPad link again, just in case anyone would like to use it:

  - https://etherpad.wikimedia.org/p/YK1nI380thDhZsi-mKZK

### Open Discussion - twenty-five minutes

[Open the discussion to any topic with the audience.]



[...]

