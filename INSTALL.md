# gitlab-ce

Install GitLab Community Edition on a virtual machine running a Debian system.


## Install system prerequisites

```sh
sudo apt update
sudo apt-get install --assume-yes ca-certificates curl openssh-server perl tzdata
sudo apt-get install --assume-yes postfix
```

## Add the gitlab-ce repository

Add the aptitude repository configuration for the gitlab-ce software package.

```sh
mkdir -p gitlab-ce
cd gitlab-ce
curl --remote-name https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh
sudo bash script.deb.sh
cd -
```

## Add the gitlab-runner repository

Add the aptitude repository configuration for the gitlab-runner software package.

```sh
mkdir -p gitlab-runner
cd gitlab-runner
curl --remote-name https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh
sudo bash script.deb.sh
cd -
```

## Install gitlab-ce

Now, install the gitlab-ce package.

```sh
sudo EXTERNAL_URL="http://${HOSTNAME}" apt install gitlab-ce
```

## Install gitlab-runner

Next, install the gitlab-runner package.

```sh
sudo apt install gitlab-runner
```

## Trust IP addresses

If necessary, enable trust for IP address CIDR ranges belonging to the virtual machine system.

```sh
sudo sed --in-place --expression="/^\# nginx\['real_ip_trusted_addresses'\] = \[\]/s|.*|&\\
# @nelsnelson $(date +"%Y-%m-%d")\\
nginx['real_ip_trusted_addresses'] = [\\
  '192.168.0.0/24',\\
  '192.168.64.3/24',\\
  '192.168.128.0/24',\\
  '192.168.0.159'\\
]|" /etc/gitlab/gitlab.rb
```

## Install docker engine

Install docker to run workloads for gitlab-runner.

```sh
# Add Docker's official GPG key:
sudo apt update
sudo install -m 0755 -d /etc/apt/keyrings
curl --fail --silent --show-error --location https://download.docker.com/linux/ubuntu/gpg --output docker.asc
sudo cp docker.asc /etc/apt/keyrings/
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update

# Install Docker
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Verify the installation
sudo docker run --rm hello-world

# Add the gitlab-runner user to the docker group:
sudo usermod --append --groups docker gitlab-runner

# Verify that gitlab-runner has access to Docker:
sudo -u gitlab-runner -H docker info

# Verify that Docker can access the gitlab server in host networking mode:
sudo docker run --rm --net=host --name curl curlimages/curl --silent "http://triton/"
```

## Runner registration

Register a runner with your gitlab instance.

```sh
source ~/.gitlabrc
sudo --preserve-env gitlab-runner register --non-interactive --url "http://triton/" --registration-token "${GITLAB_RUNNER_REGISTRATION_TOKEN}" --executor "docker" --docker-image "docker:26.0.0-dind" --docker-network-mode "host" --docker-privileged --description "docker-runner" 
```

## Enable the registry

Enable the container registry.

```sh
# Add a directive to set the registry external url, leaving the original commented line intact
sudo sed --in-place --expression="/^\# registry_external_url 'https:\/\/registry.example.com'/s|.*|&\\
registry_external_url 'http://triton'|" /etc/gitlab/gitlab.rb

# Add a directive to enable the registry, leaving the original commented line intact
sudo sed --in-place --expression="/^# gitlab_rails\['registry_enabled'\] = true/s|.*|&\\
gitlab_rails['registry_enabled'] = true|" /etc/gitlab/gitlab.rb

# Add a directive to set the registry host, leaving the original commented line intact
sudo sed --in-place --expression="/^# gitlab_rails\['registry_host'\] = \"registry.gitlab.example.com\"/s|.*|&\\
gitlab_rails['registry_host'] = \"triton\"|" /etc/gitlab/gitlab.rb

# Add a directive to set the registry port, leaving the original commented line intact
sudo sed --in-place --expression="/^# gitlab_rails\['registry_port'\] = \"5000\"/s|.*|&\\
gitlab_rails['registry_port'] = \"5000\"|" /etc/gitlab/gitlab.rb

sudo gitlab-ctl reconfigure
```

## Pulling an image

Pull an image from the local container image repository.

```sh
source ~/.gitlabrc
echo $GITLAB_REGISTRY_PASSWORD | sudo docker login --username nelsnelson@protonmail.com --password-stdin http://triton
sudo docker pull triton:5000/nelsnelson/websocket-server-jruby:latest
```
